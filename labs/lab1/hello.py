from flask import Flask
import socket as sock

## Get the hostname
if sock.gethostname().find('.') >= 0:
	hostname = sock.gethostname()
else:
	hostname = sock.gethostbyaddr(sock.gethostname())[0]

app = Flask(__name__)

@app.route("/")
def hello():
	return "Hello IoT World from pi zero: " + hostname

if __name__ == "__main__":
	app.run(host='0.0.0.0')

