This is the screenshot. I don't know why the LED commands aren't shown in the Flask/Python window,
but they still work. Funny, I had been making some changes to the lab3.js file and the browser was
caching it! Once I used a Chrome 'Incognito' window, my changes showed up. Maybe a good hint for 
class -- a bit frustrating until I figured it out. 