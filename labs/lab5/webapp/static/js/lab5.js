$(document).ready(function() {
    var led1 = "OFF";
    var led2 = "OFF";
    var led3 = "OFF";

    var cached_sensor_readings = [];
    var temp_data = [];
    var pres_data = [];

    var tempGraph = new Morris.Line({
        element: 'mytempchart',
        data: [],
        xkey: 'time',
        ykeys: ['temp'],
        labels: ['Temperature (F)']
    });

    var pressGraph = new Morris.Line({
        element: 'mypresschart',
        data: [],
        xkey: 'time',
        ykeys: ['press'],
        labels: ['Pressure (inHg)']
    });
    
    // var iotSource = new EventSource("{{ url_for('myData') }}");

    iotSource.onmessage = function(e) {
        //console.log(e.data);
        sse_data = JSON.parse(e.data);
        console.log(sse_data);

        updateSwitch(sse_data["sw"]);
        updateLeds(1, sse_data["led1"]);
        updateLeds(2, sse_data["led2"]);
        updateLeds(3, sse_data["led3"]);

        cached_sensor_readings.unshift({
            'time': Date.parse(sse_data['meas_time']['reading']),
            'temp': sse_data['temperature']['reading'],
            'press': sse_data['pressure']['reading']
        });

        while(cached_sensor_readings.length > 20)
            cached_sensor_readings.pop();

        updateDataTable();
        updateTempChart();
        updatePressChart();
    }

    function updateTempChart() {
        tempGraph.setData(cached_sensor_readings);
    }

    function updatePressChart() {
        pressGraph.setData(cached_sensor_readings);
    }

    function updateDataTable() {
        rStr = "";
        cached_sensor_readings.slice(0, 5).forEach(function(di) {
            rStr += "<tr>";
            rStr += "<td>" + di['time'] + "</td>";
            rStr += "<td>" + di['temp'] + "</td>";
            rStr += "<td>" + di['press'].toFixed(4) + "</td>";
            rStr += "</tr>";
            //console.log(di);
        });
        //console.log("updateSensorData(): -- " + rStr);
        $("tbody#sensor-data").html(rStr);
    }

    // update the Switch based on its SSE state monitor
    function updateSwitch(switchValue) {
        if (switchValue === '1') {
            // $('#switch').text(9759);
            $('#switch').toggleClass('label-default', false);
            $('#switch').toggleClass('label-success', true);
        } else if (switchValue === '0') {
            // $('#switch').text('OFF');
            $('#switch').toggleClass('label-default', true);
            $('#switch').toggleClass('label-success', false);
        }
    }

    // update the LEDs based on their SSE state monitor
    function updateLeds(ledNum, ledValue) {
        if (ledNum === 1) {
            if (ledValue === '1') {
                $('#red_led_label').toggleClass('label-default', false);
                $('#red_led_label').toggleClass('label-danger', true);
                led1 = "ON"
            } else if (ledValue === '0') {
                $('#red_led_label').toggleClass('label-default', true);
                $('#red_led_label').toggleClass('label-danger', false);
                led1 = "OFF"
            }
        } else if (ledNum === 2) {
            if (ledValue === '1') {
                $('#yel_led_label').toggleClass('label-default', false);
                $('#yel_led_label').toggleClass('label-warning', true);
                led2 = "ON"
            } else if (ledValue === '0') {
                $('#yel_led_label').toggleClass('label-default', true);
                $('#yel_led_label').toggleClass('label-warning', false);
                led2 = "OFF"
            }
        } else if (ledNum === 3) {
            if (ledValue === '1') {
                $('#grn_led_label').toggleClass('label-default', false);
                $('#grn_led_label').toggleClass('label-success', true);
                led3 = "ON"
            } else if (ledValue === '0') {
                $('#grn_led_label').toggleClass('label-default', true);
                $('#grn_led_label').toggleClass('label-success', false);
                led3 = "OFF"
            }
        }
    }

    // The button click functions run asynchronously in the browser
    $('#red_led_btn').click(function() {
        if (led1 === "OFF") { led1 = "ON"; } else { led1 = "OFF"; }
        var params = 'led=1&state=' + led1;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });

    // The button click functions run asynchronously in the browser
    $('#yel_led_btn').click(function() {
        if (led2 === "OFF") { led2 = "ON"; } else { led2 = "OFF"; }
        var params = 'led=2&state=' + led2;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });

    // The button click functions run asynchronously in the browser
    $('#grn_led_btn').click(function() {
        if (led3 === "OFF") { led3 = "ON"; } else { led3 = "OFF"; }
        var params = 'led=3&state=' + led3;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });
});
