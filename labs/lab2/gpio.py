import RPi.GPIO as GPIO

LED1_PIN = 18
LED2_PIN = 13
LED3_PIN = 23
SW_PIN = 27

class PiGpio:
    def __init__(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(LED1_PIN, GPIO.OUT)
        GPIO.setup(LED2_PIN, GPIO.OUT)
        GPIO.setup(LED3_PIN, GPIO.OUT)
        GPIO.setup(SW_PIN, GPIO.IN)
        GPIO.setup(SW_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def getSwitch(self):
        switch = GPIO.input(SW_PIN)
        if (switch == 0):
            switch = 1
        else:
            switch = 0
        return switch

    def setLed(self, led, state):
        if (state == 0) or (state == 1):
            if (led == 1):
                GPIO.output(LED1_PIN, state)
            elif (led == 2):
                GPIO.output(LED2_PIN, state)
            elif (led == 3):
                GPIO.output(LED3_PIN, state)
            else:
                pass

    def getLed(self, led):
        retVal = -1
        if (led == 1):
            retVal = GPIO.input(LED1_PIN)
        elif (led == 2):
            retVal = GPIO.input(LED2_PIN)
        elif (led == 3):
            retVal = GPIO.input(LED3_PIN)
        else:
            pass
        return retVal


