from gpio import PiGpio
from flask import *

app = Flask(__name__)
pi_gpio = PiGpio()

## Default route
@app.route('/')
def index():
    # create instance of PiGpio class (but doesn't this happen every time for default root?
    pi_gpio = PiGpio()
    swState = pi_gpio.getSwitch()
    led1State = pi_gpio.getLed(1)
    led2State = pi_gpio.getLed(2)
    led3State = pi_gpio.getLed(3)
    return render_template('index.html', switch=swState, led1=led1State, led2=led2State, led3=led3State)


# ============================== API Routes ===================================
# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://iot8e3c:5000/led/1
# curl http://iot8e3c:5000/led/2
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
  return "LED State:" + str(pi_gpio.getLed(led_state)) + "\n"


# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/sw
# -----------------------------------------------------------------------------
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pi_gpio.getSwitch()) + "!\n"

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.setLed(led,False)
    elif (state == 'ON'):
        pi_gpio.setLed(led,True)
    else:
        return "Arg Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"
    # -----------------------------------------------------------------------------

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
