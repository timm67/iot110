from flask import Flask, request
import socket

## Get the hostname
if socket.gethostname().find('.') >= 0:
    hostname = socket.gethostname()
else:
    hostname = socket.gethostbyaddr(socket.gethostname())[0]

## Instantiate Flask server
app = Flask(__name__)

# DEFAULT Route
# curl http://0.0.0.0:5000/
@app.route("/")
def default():
    return "API server default request from " + hostname + '\n'

# GET REQUEST
# curl http://0.0.0.0:5000/getHello
@app.route('/getHello')
def getRequestHello():
    return "API server GET request from " + hostname + '\n'

# POST REQUEST
# curl --data 'mykey=FOOBAR' http://0.0.0.0:5000/createHello
# echo 'mykey={"name":"Gene Cernan","age":"82"}' | curl -d @- http://0.0.0.0:5000/createHello
@app.route('/createHello', methods=['POST'])
def postRequestHello():
    mydata = request.data
    print "Data: " + mydata
    assert request.path == '/createHello'
    assert request.method == 'POST'
    data = str(request.form['mykey'])
    return 'API server ' + request.method + ' request from path ' + request.path + ' returned data: ' + data + '\n'

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')

