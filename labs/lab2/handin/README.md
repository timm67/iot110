The two images show the initial state of the system after main.py has
just been started, and a modified state after a few operations. The red and
yellow LEDs are initially ON and this is indicated in the web page. 

After pressing each button and holding down the switch, the page reveals
the new state (green ON, red and yellow OFF, and switch ON). 