SHIFT_MASK = 0x0f

class Debouncer(object):
    """ shift reg debouncer """

    def __init__(self):
        self.shiftRegister = 0;

    # perform AND logic debouncer
    def debounce(self, switchIn):
        self.shiftRegister = (self.shiftRegister << 1) | switchIn
        return int((self.shiftRegister & SHIFT_MASK) == SHIFT_MASK)

