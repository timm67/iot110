These three files show the three tabs: tabular temperature, humidity; temperature graph;
pressure graph. 

| File | Showing |
| ------------- |:-------------:| -----:|
| lab5-1.png | Tabular data |
| lab5-2.png | Temperature graph |
| lab5-3.png | Pressure graph |