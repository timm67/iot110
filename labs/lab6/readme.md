The image files in 'turnin' show the following 


| File | Showing |
| ------------- |:-------------:| -----:|
| InertialTable-python-startup.png     | Shows Python startup and complete inertial table  |
| InertialChart.png  | Chart of x,y,z acceleration values |
| EnviroTable.png    | Table of environmental values |
| EnviroChart.png    | Chart of Temp, Rel Humidity  |
